/**
 * Created by Pradhan on 6/23/16.
 */
(function () {

    $(function () {
        if ($('#skills')[0]) {
            var autoTags = ['ActionScript', 'AppleScript', 'Asp', 'BASIC', 'C', 'C++', 'CSS', 'Clojure', 'COBOL', 'ColdFusion', 'Erlang', 'Fortran', 'Groovy', 'Haskell', 'HTML', 'Java', 'JavaScript', 'Lisp', 'Perl', 'PHP', 'Python', 'Ruby', 'Scala', 'Scheme'];

            $('#skills').tagEditor({
                placeholder: 'Choose Skills...',
                autocomplete: {
                    delay: 0,
                    position: {collision: 'flip'},
                    source: autoTags
                },
                forceLowercase: false,

            });
        }

        if ($('#skills-search')[0]) {
            $('#skills-search').tagEditor({
                placeholder: 'Search by Skills',
            });
        }
    });

})(jQuery)