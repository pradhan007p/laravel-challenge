<?php
/**
 * Created by PhpStorm.
 * User: Pradhan
 * Date: 6/23/16
 * Time: 12:53 PM
 */

namespace App\Models;

use DB;
use Mail;
use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table = 'jobs';
    protected $primaryKey = 'job_id';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'email', 'job_title', 'job_description', 'job_token', 'skills'
    ];

    public function saveJob($data, $token)
    {
        $job = Jobs::firstOrCreate(['job_token' => $token]);

        if ($token == '' || !$token) {
            $token = md5(uniqid());
        }

        $job->job_token = $token;
        $job->email = $data['email'];
        $job->job_title = $data['job_title'];
        $job->job_description = $data['job_description'];
        $job->skills = $data['skills'];

        if ($job->save()) {
            $this->sendEmail($data['email'], $token);
            return true;
        } else {
            return false;
        }

    }

    public function searchJobs($inputs, $paginate = 50)
    {
        $data = DB::table('jobs');
        $inputs = array_filter($inputs);

        //Filter for search query
        if (!empty($inputs)) {

            $data = $data->where(function ($query) use ($inputs) {
                foreach ($inputs as $key => $term) {

                    if ($key == 'skills') {
                        $skills = explode(',', $term);
                        $where = '';
                        foreach ($skills as $skill) {
                            $where .= "FIND_IN_SET('$skill', skills) > 0 OR ";
                        }
                        $where = rtrim($where, ' OR ');

                        $query->whereRaw('('.$where.')');

                    } else {
                        if ($term) {
                            $query->where('job_title', 'LIKE', '%' . $term . '%');
                        }
                    }
                }
            });
        }
        $data = $data->paginate($paginate);



        return $data;
    }


    function deleteOldJobs()
    {
        $status = DB::table('jobs')
            ->whereRaw('DATE(created_at) < DATE(NOW()-INTERVAL 30 DAY)')
            ->delete();

        return $status;
    }

    private function sendEmail($email, $token)
    {
        $data = [
            'link' => url('jobs/add-new/' . $token),
        ];

        try {

            Mail::send('emails.job', $data, function ($m) use ($email) {
                $m->from('pradhan@pradhanp.com.np', 'Job Created');

                $m->to($email)->subject('Job Created');
            });

        } catch (Exception $e) {
            var_dump($e);
        }
    }
}