<?php
/**
 * Created by PhpStorm.
 * User: Pradhan
 * Date: 6/23/16
 * Time: 9:48 AM
 */

namespace App\Http\Controllers;


use App\Models\Jobs;
use Illuminate\Http\Request;

class JobController extends Controller
{

    function getIndex()
    {
        $data = [
            'search' => '',
            'jobs' => Jobs::paginate(50)
        ];
        return view('jobs')->with($data);
    }

    function getAddNew($jobToken = '')
    {
        $data = [
            'jobToken' => $jobToken,
            'job' => Jobs::where('job_token', $jobToken)->first()
        ];
        return view('add-job')->with($data);
    }

    function postSaveJobs(Request $request, $jobToken = 0)
    {

        $rules = [
            'email' => 'required|email',
            'job_title' => 'required',
            'job_description' => 'required',
            'skills' => 'required'
        ];

        //Validate Required Fields
        $this->validate($request, $rules);


        $job = new Jobs();
        if ($job->saveJob($request->except('_token'), $jobToken)) {
            return redirect('jobs')->with('status', 'Job has been saved successfully!');

        } else {
            return redirect()->back()->withInputs()->withErrors('Failed to save job , Please try again!');
        }

    }

    public function getSearch(Request $request)
    {
        $job = new Jobs();
        $inputs = $request->except('_token');
        $data = [
            'search' => $inputs,
            'jobs' => $job->searchJobs($inputs)
        ];
        return view('jobs')->with($data);
    }


    public function getDelete($jobToken = '')
    {

        if (Jobs::where('job_token', $jobToken)->delete()) {
            return redirect('jobs')->with('status', 'Job has been deleted successfully!');

        } else {
            return redirect()->back()->withErrors('Failed to delete job , Please try again!');
        }
    }

    //this function will be called by cron to trigger old jobs delet
    public function getDeleteOldJobs()
    {
        $job = new Jobs();
        $job->deleteOldJobs();

        return redirect('jobs')->with('status', 'Job has been deleted successfully!');
    }

}