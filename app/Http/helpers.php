<?php
function splitTags($skills) {
    $skills = explode(',', $skills);

    $html = '';
    foreach($skills as $skill){
        $html .= "<label class='label skills label-info'>$skill</label> ";
    }

    return $html;
}