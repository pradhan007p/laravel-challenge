<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Test Jobs</title>

    <!-- Bootstrap core CSS -->
    {!!  Html::style('/assets/bootstrap/css/bootstrap.min.css')  !!}
    {!!  Html::style('/assets/css/jquery.tag-editor.css')  !!}
    {!!  Html::style('/assets/css/styles.css')  !!}

            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('jobs') }}">Test Jobs</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{ url('jobs') }}">Jobs</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container">


    @yield('content')

</div><!-- /.container -->

<!-- Bootstrap core JavaScript
================================================== -->
{!!  Html::script('assets/js/jquery-1.11.3.min.js')  !!}
{!!  Html::script('assets/bootstrap/js/bootstrap.min.js')  !!}
{!!  Html::script('https://code.jquery.com/ui/1.10.2/jquery-ui.min.js')  !!}
{!!  Html::script('assets/js/jquery.caret.min.js')  !!}
{!!  Html::script('assets/js/jquery.tag-editor.min.js')  !!}
        <!-- Custom JS -->
{!!  Html::script('assets/js/site.js')  !!}

</body>
</html>
