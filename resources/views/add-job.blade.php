@extends('zlayout.template')

@section('content')

    <h2>{{ ($jobToken && $jobToken != '')?'Edit':'Add' }} Job</h2>

    @include('zlayout.alerts')

    <div class="well">
        {!! Form::model($job, ['url' => 'jobs/save-jobs/'.$jobToken, 'class' =>'form-horizontal']) !!}


        <div class="form-group">
            {!! Form::label('job_title', 'Job Title',['class' => 'control-label']) !!}
            {!! Form::text('job_title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email Address',['class' => 'control-label']) !!}
            {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('skills', 'Skills',['class' => 'control-label skill-label tag-editor']) !!}
            {!! Form::textarea('skills', null, ['class' => 'form-control', 'required' => 'required', 'rows' => '1']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('job_description', 'Job Description',['class' => 'control-label']) !!}
            {!! Form::textarea('job_description', null, ['class' => 'form-control', 'required' => 'required', 'rows' => '5']) !!}
        </div>

        <div class="form-group">
            <button class="btn btn-primary">Save Job</button>
            <a href="{{ url('jobs') }}" class="btn btn-default">Cancel</a>

            @if($jobToken && $jobToken != '')
                <div class="pull-right">
                    <a onclick="return confirm('Are you sure to delete this job?')"
                       href="{{ url('jobs/delete/'.$jobToken) }}" class="btn btn-danger">Delete</a>
                </div>
            @endif

            <div class="clearfix"></div>
        </div>


        {!! Form::close() !!}
    </div>
@endsection