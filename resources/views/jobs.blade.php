@extends('zlayout.template')

@section('content')

    <h3>Job Lists</h3>

    @include('zlayout.alerts')

    <div class="well">
        <div class="col-sm-10">
            {!! Form::model($search, ['url' => 'jobs/search', 'method'=>'get','class' => 'row']) !!}
            <div class="col-sm-7">
                {!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'Search by Job Title']) !!}
            </div>
            <div class="col-sm-4">
                {!! Form::text('skills', null, ['class' => 'form-control', 'id' => 'skills-search', 'placeholder' => 'Search by Skills...']) !!}
            </div>
            <div class="col-sm-1">
                <button class="btn btn-success">
                    Search
                </button>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-sm-2">
            <a class="btn btn-primary btn-block" href="{{ url('jobs/add-new') }}">
                <i class="glyphicon glyphicon-plus"></i> Create New Job
            </a>
        </div>

        <div class="clearfix"></div>
    </div>


    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Job Title</th>
            <th>Email</th>
            <th>Skills</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 0; ?>
        @forelse($jobs as $job)
            <?php $i++; ?>
            <tr>
                <td>
                    {{ $i }}
                </td>
                <td>
                    {{ $job->job_title }}
                </td>
                <td>
                    {{ $job->email }}
                </td>
                <td>
                    {!!  splitTags($job->skills)  !!}
                </td>
                <td>
                    {{ $job->created_at }}
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5">
                    No Jobs Found
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>

    {!! $jobs->render() !!}
@endsection