<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class JobTest extends TestCase
{

    use DatabaseTransactions;

    public function testNewJob()
    {
        $this->visit('/jobs/add-new')
            ->type('TEST JOB', 'job_title')
            ->type('hello1@in.com', 'email')
            ->type('php,laravel', 'skills')
            ->type('Test Description', 'job_description')
            ->press('Save Job')
            ->seePageIs('/jobs');
    }

}