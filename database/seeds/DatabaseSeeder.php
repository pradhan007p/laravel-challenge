<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([
            [
                'email' => 'user1@gmail.com',
                'job_title' => 'Cras in sapien',
                'job_description' => 'Vivamus sed orci in est tempus vehicula nec quis diam. Cras in sapien eget nisl accumsan consectetur. Maecenas gravida eget neque sed efficitur. Nunc commodo, lacus id eleifend porta, tellus dui volutpat lectus, sit amet ullamcorper massa sem vel felis. Donec faucibus scelerisque ligula sed maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris ultrices, felis quis gravida malesuada, magna ipsum faucibus tortor, non feugiat ipsum mi a enim.',
                'job_token' => md5(uniqid()),
                'location' => 'Dhulikhel',
                'skills' => 'php,laravel'
            ],
            [
                'email' => 'user2@gmail.com',
                'job_title' => 'Donec in sapien',
                'job_description' => 'Quisque id ultrices elit. Donec ante velit, molestie sit amet cursus et, sagittis nec quam. Duis lectus velit, lobortis et purus a, fringilla commodo mauris. Integer vel felis ut augue pretium gravida at vel odio. Morbi semper varius ipsum. Proin bibendum ac magna sit amet placerat.',
                'job_token' => md5(uniqid()),
                'location' => 'Banepa',
                'skills' => 'javascrpit, photoshop, php'
            ],
            [
                'email' => 'user3@gmail.com',
                'job_title' => 'Donec faucibus sapien',
                'job_description' => 'Cras in sapien eget nisl accumsan consectetur. Maecenas gravida eget neque sed efficitur. Nunc commodo, lacus id eleifend porta, tellus dui volutpat lectus, sit amet ullamcorper massa sem vel felis. Donec faucibus scelerisque ligula sed maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris ultrices, felis quis gravida malesuada, magna ipsum faucibus tortor, non feugiat ipsum mi a enim.',
                'job_token' => md5(uniqid()),
                'location' => 'Kathmandu',
                'skills' => 'laravel, c#, java'
            ],
            [
                'email' => 'user4@gmail.com',
                'job_title' => 'Maecenas gravida eget',
                'job_description' => ' Maecenas gravida eget neque sed efficitur. Nunc commodo, lacus id eleifend porta, tellus dui volutpat lectus, sit amet ullamcorper massa sem vel felis. Donec faucibus scelerisque ligula sed maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris ultrices, felis quis gravida malesuada, magna ipsum faucibus tortor, non feugiat ipsum mi a enim.',
                'job_token' => md5(uniqid()),
                'location' => 'Kathmandu',
                'skills' => 'codeigniter, php, laravel'
            ],
            [
                'email' => 'user5@gmail.com',
                'job_title' => 'Nunc gravida eget',
                'job_description' => 'Nunc commodo, lacus id eleifend porta, tellus dui volutpat lectus, sit amet ullamcorper massa sem vel felis. Donec faucibus scelerisque ligula sed maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris ultrices, felis quis gravida malesuada, magna ipsum faucibus tortor, non feugiat ipsum mi a enim.',
                'job_token' => md5(uniqid()),
                'location' => 'Dhulikhel',
                'skills' => 'ios,android,ionic'
            ],
            [
                'email' => 'user6@gmail.com',
                'job_title' => 'Vestibulum Maecenas eget',
                'job_description' => ' Maecenas gravida eget neque sed efficitur. Nunc commodo, lacus id eleifend porta, tellus dui volutpat lectus, sit amet ullamcorper massa sem vel felis. Donec faucibus scelerisque ligula sed maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris ultrices, felis quis gravida malesuada, magna ipsum faucibus tortor, non feugiat ipsum mi a enim.',
                'job_token' => md5(uniqid()),
                'location' => 'Banepa',
                'skills' => 'jquery,laravel'
            ],
            [
                'email' => 'user7@gmail.com',
                'job_title' => ' Faucibus luctus maecenas',
                'job_description' => 'Nunc commodo, lacus id eleifend porta, tellus dui volutpat lectus, sit amet ullamcorper massa sem vel felis. Donec faucibus scelerisque ligula sed maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris ultrices, felis quis gravida malesuada, magna ipsum faucibus tortor, non feugiat ipsum mi a enim.',
                'job_token' => md5(uniqid()),
                'location' => 'Dhulikhel',
                'skills' => 'php,asp.net'
            ]
        ]);
    }
}
